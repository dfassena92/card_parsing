import 'package:flutter/material.dart';

void main() {
  runApp(new MaterialApp(
    title: "card & Parsing",
    home: new HalSatu(),
  ));
}

class HalSatu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(title: new Text("Card & Parsing"),),
      body: new Container(
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[

            new Card(
              child: 
              new Column(
                children: <Widget>[
                  new Icon(Icons.home, size: 50.0, color: Colors.black,),
                  new Text("Home", style: new TextStyle(fontSize: 20.0),)
                ],
              )
            ),

            new Card(
              child: 
              new Column(
                children: <Widget>[
                  new Icon(Icons.home, size: 50.0, color: Colors.black,),
                  new Text("Home", style: new TextStyle(fontSize: 20.0),)
                ],
              )
            ),

            new Card(
              child: 
              new Column(
                children: <Widget>[
                  new Icon(Icons.home, size: 50.0, color: Colors.black,),
                  new Text("Home", style: new TextStyle(fontSize: 20.0),)
                ],
              )
            ),

            new Card(
              child: 
              new Column(
                children: <Widget>[
                  new Icon(Icons.home, size: 50.0, color: Colors.black,),
                  new Text("Home", style: new TextStyle(fontSize: 20.0),)
                ],
              )
            ),

          ],
        ),
      ),
    );
  }
}